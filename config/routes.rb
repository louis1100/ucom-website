Rails.application.routes.draw do

  resources :resumes, only: [:index, :new, :create, :destroy]

  get 'resumes/index'

  get 'resumes/new'

  get 'resumes/create'

  get 'resumes/destroy'

  devise_for :users
  resources :articles

  resources :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "pages#home"
  get '/contact' => 'pages#contact'

end
