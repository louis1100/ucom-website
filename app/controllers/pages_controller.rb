class PagesController < ApplicationController

	def home
		@home_dir = String(Rails.root) + '/app'
		@home_info_file = 'home_info'
		@lastest_projects = Article.limit(10).order('created_at desc')

		@page_info = JSON.parse File.open(@home_dir + "/assets/jsons/#{@home_info_file}.json").read
	end

	def index_users
		
	end

	def contact

	end

	def login

	end

	def register

	end

end
