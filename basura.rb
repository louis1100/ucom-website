#require SecureRandom

words = %w"learning lollipop education image computer mobile"

total_chances = 5
wrong_try = 0
right_guess = ''

hanged = <<HANG
 +---+-
 |   |
 |   0
 |  /|\\
 |  //
-+--------
HANG

survived = <<WIN
You Win
WIN

word = words[rand(words.length) - 1]

def get_placeholder sample_word, guessed_words
  placeholder = ''
  sample_word.chars { |char|
    placeholder += (guessed_words.include? char)? char: "#"
  }

  placeholder
end

puts `cls`
puts 'Guess what is: ' + get_placeholder(word, '')

while true
  print "Enter letter [#{total_chances - wrong_try} changes left]:"
  
  char = gets.chomp
  puts `cls`

  if word.include? char
    if(right_guess.include? char)
      puts char + " is already given and accepted."
      puts "Try another: " + get_placeholder(word, right_guess)
    else
      right_guess = right_guess + char
      placeholder = get_placeholder(word, right_guess)

      puts "Great! " + placeholder
    end

    unless placeholder.include? "#"
      puts "Good. You made it. Yay"
      puts survived
      break
    end

  else
    puts "Sorry. The word doesn't contain '#{char}'"
    wrong_try += 1

    if(wrong_try == total_chances)
      puts "You hanged!"
      puts hanged
      break
    else
      puts "Try another: " + get_placeholder(word, right_guess)
    end
  end
end

puts "The word was '#{word}'"